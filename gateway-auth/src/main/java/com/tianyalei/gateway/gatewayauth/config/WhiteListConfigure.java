package com.tianyalei.gateway.gatewayauth.config;

import com.tianyalei.gateway.gatewayauth.gateway.filter.WhileListFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.gateway.config.GatewayAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wuweifeng wrote on 2019-08-19.
 */
@Configuration
@ConditionalOnMissingBean(WhiteListConfigure.class)
@ConditionalOnClass(GatewayAutoConfiguration.class)
public class WhiteListConfigure {

    @Bean
    public WhileListFilter whileListFilter() {
        return new WhileListFilter();
    }
    
}
