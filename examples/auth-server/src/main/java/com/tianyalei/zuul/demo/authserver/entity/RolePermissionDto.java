package com.tianyalei.zuul.demo.authserver.entity;

import java.io.Serializable;
import java.util.List;

public class RolePermissionDto implements Serializable {
    private long roleId;

    private List<Permission> permissionList;

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public List<Permission> getPermissionList() {
        return permissionList;
    }

    public void setPermissionList(List<Permission> permissionList) {
        this.permissionList = permissionList;
    }
}
