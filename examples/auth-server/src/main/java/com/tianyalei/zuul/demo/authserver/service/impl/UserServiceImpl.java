
package com.tianyalei.zuul.demo.authserver.service.impl;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tianyalei.zuul.demo.authserver.dao.UserDao;
import com.tianyalei.zuul.demo.authserver.entity.UserEntity;
import com.tianyalei.zuul.demo.authserver.form.LoginForm;
import com.tianyalei.zuul.demo.authserver.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;



@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {

	@Override
	public UserEntity queryByMobile(String mobile) {
		return baseMapper.selectOne(new QueryWrapper<UserEntity>().eq("mobile", mobile));
	}

	@Override
	public long login(LoginForm form) {
		UserEntity user = queryByMobile(form.getMobile());
	/*	Assert.isNull(user, "手机号或密码错误");

		//密码错误
		if(!user.getPassword().equals(DigestUtils.sha256Hex(form.getPassword()))){
			throw new RuntimeException("手机号或密码错误");
		}*/

		return user.getUserId();
	}

	public static void main(String[] args) {
		Assert.isNull("123","ahahah");
	}
}
