package com.tianyalei.zuul.demo.authserver.controller;

import com.tianyalei.zuul.demo.authserver.entity.Role;
import com.tianyalei.zuul.demo.authserver.entity.RolePermissionDto;
import com.tianyalei.zuul.demo.authserver.entity.Permission;

import com.tianyalei.zuul.demo.authserver.entity.UserRoleDto;
import com.tianyalei.zuul.zuulauth.cache.AuthCache;
import com.tianyalei.zuul.zuulauth.tool.FastJsonUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("permission")
public class PermissionController {

    @Autowired
    AuthCache authCache;


    @Autowired
    @Qualifier("stringRedisTemplate")
    StringRedisTemplate stringRedisTemplate;

    @PostMapping("saveRolePermission")
    public void saveRolePermission(@RequestBody RolePermissionDto rolePermissionDto){
        long roleId = rolePermissionDto.getRoleId();
        List<Permission> permissionList = rolePermissionDto.getPermissionList();
        removeRolePermissionByRoleId(roleId);
        stringRedisTemplate.opsForValue().set("role:"+roleId, FastJsonUtils.convertObjectToJSON(permissionList));
        authCache.saveRolePermission(roleId+"",permissionList.stream().map(Permission::getUri).collect(Collectors.toSet()));
    }

    private void removeRolePermissionByRoleId(long roleId){
        stringRedisTemplate.delete("role:"+roleId);
        authCache.removeRolePermission(roleId+"");
    }

    @RequestMapping("getPermissionsByRoleId")
    public List<Permission> getPermissionsByRoleId(long roleId){
        String permission = stringRedisTemplate.opsForValue().get(String.valueOf(roleId));
        if (permission ==null){
            return new ArrayList<>();
        }
        return FastJsonUtils.toList(permission, Permission.class);
    }


    @PostMapping("saveUserRoles")
    public void saveUserRoles(@RequestBody UserRoleDto userRoleDto){
        long userId = userRoleDto.getUserId();
        List<Role> roles = userRoleDto.getRoleList();
        removeUserRolesByUserId(userId);
        stringRedisTemplate.opsForValue().set("userId:"+userId, FastJsonUtils.convertObjectToJSON(roles));

        Set<String> roleIdSet = new HashSet<>();
        for (Role role : roles) {
            roleIdSet.add(String.valueOf(role.getRoleId()));
        }
        authCache.saveUserRole(userId+"",roleIdSet);
    }

    private void removeUserRolesByUserId(long userId){
        stringRedisTemplate.delete("userId:"+userId);
        authCache.removeUserRole(userId+"");
    }

    @RequestMapping("getUserRolesByUserId")
    public List<Role> getUserRolesByUserId(long userId){
        String roleStr = stringRedisTemplate.opsForValue().get("userId:"+userId);
        if (roleStr ==null){
            return new ArrayList<>();
        }
        return FastJsonUtils.toList(roleStr, Role.class);
    }


    @PostMapping("saveAppPermissions")
    public void saveAppPermissions(@RequestBody RolePermissionDto rolePermissionDto){
        long roleId = rolePermissionDto.getRoleId();
        List<Permission> permissionList = rolePermissionDto.getPermissionList();
        stringRedisTemplate.opsForValue().set(String.valueOf(roleId), FastJsonUtils.convertObjectToJSON(permissionList));
        authCache.saveRolePermission(roleId+"",permissionList.stream().map(Permission::getUri).collect(Collectors.toSet()));
    }


}
