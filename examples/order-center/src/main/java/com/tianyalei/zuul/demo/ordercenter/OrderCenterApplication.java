package com.tianyalei.zuul.demo.ordercenter;


import com.tianyalei.zuul.zuulauth.annotation.EnableClientAuth;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 基于zuul的demo
 * demo工程-订单服务 用于测试权限
 * @author ratel 2022-04-10
 */
@SpringBootApplication
@EnableClientAuth
public class OrderCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderCenterApplication.class, args);
        System.out.println("启动成功!");
    }

}
